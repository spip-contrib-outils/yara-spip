#!/bin/bash
SPIP_GIT_URL="https://git.spip.net/"  #Must be a git clone
SPIP_GIT_REPO="SPIP/spip.git"
DIST_GIT_ORGA="dist"

PHP_MALWARE_FINDER_GIT_SOURCE="https://github.com/nbs-system/php-malware-finder.git"

SPIP_DIRECTORY="spip"
DIST_DIRECTORY="$SPIP_DIRECTORY/plugins-dist"
PHP_MALWARE_DIRECTORY="php-malware-finder"

GENERATE_WHITELIST_SCRIPT="${PHP_MALWARE_DIRECTORY}/php-malware-finder/utils/generate_whitelist.py"
YARA_WHITELISTS_DIRECTORY="whitelists"
YARA_CONDITIONS=""
YARA_RULE_NAME="Spip"

#check script presence
#git
#python-yara

if [[ -d "$SPIP_DIRECTORY" && -d "$SPIP_DIRECTORY/.git" ]]; then
	git -C $SPIP_DIRECTORY clean -fd
	git -C $SPIP_DIRECTORY fetch --all
else
	rm -fr "./$SPIP_DIRECTORY"
	git clone "$SPIP_GIT_URL/$SPIP_GIT_REPO" "$SPIP_DIRECTORY"
fi

curl -s -X GET "https://git.spip.net/api/v1/orgs/$DIST_GIT_ORGA/repos" -H  "accept: application/json" -H  "Authorization: $TOKEN" -o repos.json
last_index_repo=$(jq -r 'length - 1' repos.json)

for i in $(seq 0 "$last_index_repo")
do
	repo_git=$(jq -r ".[$i] .html_url" repos.json)
	repo_name=$(jq -r ".[$i] .name" repos.json)
	echo "Import $repo_name"

	if [ ! -d "$DIST_DIRECTORY/$repo_name" ]; then
		git clone "$repo_git" "$DIST_DIRECTORY/$repo_name"
	else
		git -C "$DIST_DIRECTORY/$repo_name" fetch --all --tags --prune
	fi
done

if [[ -d "$PHP_MALWARE_DIRECTORY" && -d "$PHP_MALWARE_DIRECTORY/.git" ]]; then
	git -C $PHP_MALWARE_DIRECTORY clean -fd
	git -C $PHP_MALWARE_DIRECTORY reset --hard HEAD
	git -C $PHP_MALWARE_DIRECTORY fetch --all
else
	rm -fr "./$PHP_MALWARE_DIRECTORY"
	git clone "$PHP_MALWARE_FINDER_GIT_SOURCE" "$PHP_MALWARE_DIRECTORY"
fi

if [[ -d "$YARA_WHITELISTS_DIRECTORY" ]]; then
	rm -r "./$YARA_WHITELISTS_DIRECTORY"
fi
mkdir -p "$YARA_WHITELISTS_DIRECTORY/$YARA_RULE_NAME"

#Workaround https://github.com/nbs-system/php-malware-finder/issues/88
#sed -i '95s#.*#$pr = /(preg_replace(_callback)?|mb_ereg_replace|preg_filter)\\s*\\(.{1,1000}(\\/|\\\\x2f)(e|\\\\x65)['\''"]/  nocase // http://php.net/manual/en/function.preg-replace.php#' "$PHP_MALWARE_DIRECTORY/php-malware-finder/php.yar"
sed -i '22s#error_on_warning=True#error_on_warning=False#' "$GENERATE_WHITELIST_SCRIPT"
#Set private rule
sed -i '38s#\\nrule#\\nprivate rule#' "$GENERATE_WHITELIST_SCRIPT"

git -C $SPIP_DIRECTORY fetch --all --tags --prune

for tag in $(git -C "$SPIP_DIRECTORY" tag -l)
do
	spip_version="${tag:5}" #Remove spip- prefix
	yara_rule_name=${spip_version//.} #remove dot
	yara_rule_name=${yara_rule_name//-} #remove minus
	yara_rule_name=$YARA_RULE_NAME$yara_rule_name

	git -C $SPIP_DIRECTORY checkout -f "tags/$tag"
	for plugin in "$DIST_DIRECTORY"/*
	do
		if [ ! -d "$plugin" ];then
			continue;
		fi
		git -C "$plugin" checkout -f "spip/$spip_version" >/dev/null 2>/dev/null
		if [ "$?" != "0" ];then
			git -C "$plugin" rm -rf .
			git -C "$plugin" clean -fxd
		fi
	done
	$GENERATE_WHITELIST_SCRIPT "$yara_rule_name" "$SPIP_DIRECTORY" > "$YARA_WHITELISTS_DIRECTORY/$YARA_RULE_NAME/$tag.yar"
	echo "include \"$YARA_RULE_NAME/$tag.yar\"" >> "$YARA_WHITELISTS_DIRECTORY/$YARA_RULE_NAME.yar"
	YARA_CONDITIONS="$YARA_CONDITIONS\\n        $yara_rule_name or"
done

echo -e "private rule $YARA_RULE_NAME\\n{    condition:${YARA_CONDITIONS::-3}\\n}" >> "$YARA_WHITELISTS_DIRECTORY/$YARA_RULE_NAME.yar"

find "$YARA_WHITELISTS_DIRECTORY" -iname "*.yar" -exec sed -i '/\.git/d' {} \;
